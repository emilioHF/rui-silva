﻿using System;

namespace PracticaCirculo
{
    class Program
    {
        static void Main(string[] args)
        {   //primero se declara las variables, numero pi es constante por que nunca cambia
            double radio, longitud, area;
            const double Pi = 3.14;
            string captura;
            // Luego se pregunta cual es el radio del Círculo pero tenemos que cambiarla a un string
            Console.Write("\n\t ¿Radio del circulo?: ");
            captura = Console.ReadLine();
            radio = Convert.ToDouble(captura);
            //luego se pone las formulas matematicas,las cual se resuelve atumaticamente al poner el radio.

            area = Pi * radio * radio;
            longitud = 2 * Pi * radio;
            //luego solicitamos por consola los datos totales del circulo usando write o writeline

            Console.Write("\t\n Datos del un círculo de radio = " + radio);
            Console.WriteLine("\n\t Area es = " + area);
            Console.WriteLine("\n\t longitud es = " + longitud);


            Console.ReadKey();
            











        }
    }
}
